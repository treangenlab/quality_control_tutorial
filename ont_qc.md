
## Workflow Overview
The workflow starts from raw fast5 signal from Oxford Nanopore Technology sequencing machines (PromethION). No adapter trimming is required seperatly since Guppy software provided by ONT has already automatically doing that. NanoPlot is a long read statistics plotting method to obtain an overview of the base called reads. 

![Workflow Overview](/images/ontqc.png)

## Input Data

The data is from the paper[1], and fast5 signal file is at https://www.ebi.ac.uk/ena/browser/view/ERR2887850. With the signal file, we can apply the base calling with Guppy. Guppy can be found at https://community.nanoporetech.com/downloads/. The version of Guppy (CPU/GPU) depends on the hardware that users have. The Nanopore version of the dataset above is GridION and the selection of the Guppy configuration is ```dna_r9.4.1_450bps_fast.cfg```. 

The command of the guppy is ```guppy_basecaller -i <input path> -s <save path> -c <config file> [options]```. The command would take a very long time to run if on CPU, while GPU can sigificantly speed up the process (a one-day basecalling work on CPU can be done in one hour on a Titan RTX GPU https://esr-nz.github.io/gpu_basecalling_testing/gpu_benchmarking.html). The guppy processed fastq file is also provided at ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR315/006/ERR3152366/ERR3152366.fastq.gz. 

To choose the configure file, one need to inspect the ONT kit name and the ```cfg``` file name. For example, a config file for high accuracy DNA basecalling on an R9.4.1 pore at 450 bases per second would be called dna_r9.4.1_450bps_hac.cfg. It may not be clear what pore and speed you are using, which is why directly choosing a config file is generally left to expert users. All other users should select flow cell and kit instead. The config files are named as ```<strand type>_<pore version>_<speed>_<custom tags>.cfg```[3]. 

Guppy base calling options can be divided into 4 modes with different configuration files:

- Super-accurate basecalling
- High Accuracy basecalling (set by default when a kit and flow cell are chosen)
- Fast basecalling
- Modified basecalling 

The fast basecalling mode is trading off basecalling accuracy for a much faster running process. Example ```cfg``` files are in the [ont_cfgs](/ont_cfgs) folder.

Bonito basecaller (https://github.com/nanoporetech/bonito) is also provided by ONT and it is mainly optimized with GPU. Bonito provides the ability for model training from scratch, and base modification calling. 

## Nanoplot Report Generation

Use NanoPlot[2] command ```NanoPlot -t 16 --N50 --fastq ERR3152366.fastq.gz``` to generate a report file for the fastq reads. The result web file is [here](https://glcdn.githack.com/treangenlab/quality_control_tutorial/-/raw/main/NanoPlot-report.html). 

The result contains basic stats like Number, percentage and megabases of reads above quality cutoffs![q_qual](/images/ont_q_cutoff.png), also a read length distribution plot with N50 mark. ![n50](/images/n50_plot.png)

Other useful information in the report contains general summaries and plots indicate read length vs read qualities.


## Citations

[1] Samuel M Nicholls, Joshua C Quick, Shuiquan Tang, Nicholas J Loman, Ultra-deep, long-read nanopore sequencing of mock microbial community standards, GigaScience, Volume 8, Issue 5, May 2019, giz043, https://doi.org/10.1093/gigascience/giz043

[2] Wouter De Coster, Svenn D’Hert, Darrin T Schultz, Marc Cruts, Christine Van Broeckhoven, NanoPack: visualizing and processing long-read sequencing data, Bioinformatics, Volume 34, Issue 15, 01 August 2018, Pages 2666–2669, https://doi.org/10.1093/bioinformatics/bty149

[3] Guppy Protocol: https://community.nanoporetech.com/docs/prepare/library_prep_protocols/Guppy-protocol/v/gpb_2003_v1_revae_14dec2018/setting-up-a-run-configurations-and-parameters
